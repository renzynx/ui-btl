import Button from "@/app/_components/Button";
import Link from "next/link";
import React from "react";

const Login = () => {
  return (
    <div className="flex justify-center items-center flex-col mt-20">
      <div className="mt-4 flex flex-col items-center justify-center bg-zinc-800 p-5 gap-5 max-w-md w-full rounded-md shadow-md">
        <h1 className="text-2xl font-bold">Login</h1>
        <div className="flex flex-col w-full">
          <label htmlFor="email">Email</label>
          <input
            id="email"
            className="border border-zinc-900 p-2 text-black"
            type="text"
            placeholder="Email"
          />
        </div>

        <div className="flex flex-col w-full">
          <label htmlFor="password">Password</label>
          <input
            id="password"
            className="border border-zinc-900 p-2 text-black"
            type="password"
            placeholder="Password"
          />
        </div>

        <div className="w-full flex justify-between items-center">
          <Link href="/auth/register" passHref>
            <p className="text-sm underline cursor-pointer">
              Don&apos;t have an account?
            </p>
          </Link>
          <Button className="px-5 py-2">Login</Button>
        </div>
      </div>
    </div>
  );
};

export default Login;
