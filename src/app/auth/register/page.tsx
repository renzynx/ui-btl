"use client";
import Button from "@/app/_components/Button";
import Link from "next/link";
import React from "react";
import axios from "axios";
import { API_URL } from "@/app/_libs/constant";
import { AuthResponse } from "@/app/_libs/interfaces";
import Router from "next/router";

const Register = () => {
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [confirmPassword, setConfirmPassword] = React.useState("");

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  };

  const handleConfirmPasswordChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setConfirmPassword(e.target.value);
  };

  const register = async () => {
    const response = await axios.post<AuthResponse>(
      API_URL + "/User/register",
      {
        email,
        password,
      }
    );

    if (response.data.token) {
      localStorage.setItem("token", response.data.token);
      window.location.href = "/";
    } else {
      console.log(response.status);
    }
  };

  return (
    <div className="flex justify-center items-center flex-col mt-20">
      <div className="mt-4 flex flex-col items-center justify-center bg-zinc-800 p-5 gap-5 max-w-md w-full rounded-md shadow-md">
        <h1 className="text-2xl font-bold">Register</h1>
        <div className="flex flex-col w-full">
          <label htmlFor="email">Email</label>
          <input
            id="email"
            className="border border-zinc-900 p-2 text-black"
            type="text"
            placeholder="Email"
            onChange={handleEmailChange}
          />
        </div>

        <div className="flex flex-col w-full">
          <label htmlFor="password">Password</label>
          <input
            id="password"
            className="border border-zinc-900 p-2 text-black"
            type="password"
            placeholder="Password"
            onChange={handlePasswordChange}
          />
        </div>

        <div className="flex flex-col w-full">
          <label htmlFor="password">Confirm Password</label>
          <input
            id="password"
            className="border border-zinc-900 p-2 text-black"
            type="password"
            placeholder="Confirm Password"
            onChange={handleConfirmPasswordChange}
          />
        </div>

        <div className="w-full flex justify-between items-center">
          <Link href="/auth/login" passHref>
            <p className="text-sm underline cursor-pointer">
              Already have an account?
            </p>
          </Link>
          <Button className="px-5 py-2" onClick={register}>
            Register
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Register;
