import { ButtonHTMLAttributes, ComponentPropsWithoutRef, FC } from "react";

const Button: FC<ComponentPropsWithoutRef<"button">> = ({ ...props }) => {
  return (
    <button
      {...props}
      className={`${props.className} px-3 py-2 rounded-md bg-white text-black shadow-lg hover:bg-white/95 hover:text-black/95 ease-in-out`}
    >
      {props.children}
    </button>
  );
};

export default Button;
