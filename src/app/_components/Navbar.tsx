/* eslint-disable react/no-children-prop */
"use client";
import { IconShoppingCart, IconSearch, IconMenu2 } from "@tabler/icons-react";
import Button from "./Button";
import Drawer from "./Drawer";
import Image from "next/image";
import axios from "axios";
import { API_URL } from "../_libs/constant";
import { useEffect } from "react";

const Navbar = () => {
  useEffect(() => {
    axios
      .get(API_URL + "/User/self", {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      })
      .then((res) => res.data)
      .then(console.log);
  }, []);

  return (
    <nav className="flex justify-center items-center border-b-[.5px] border-b-zinc-800">
      <div className="container flex justify-between p-5">
        <div className="lg:hidden md:hidden p-2 ring-1 ring-zinc-700 shadow-sm cursor-pointer rounded-md flex justify-center items-center hover:bg-zinc-900 ease-out">
          <Drawer
            controller={<IconMenu2 />}
            children={
              <div className="flex flex-col gap-6">
                <div className="relative">
                  <span className="absolute inset-y-0 right-0 flex items-center pr-2">
                    <IconSearch size={18} />
                  </span>
                  <input
                    placeholder="Tìm kiếm..."
                    className="w-full rounded-lg border bg-white px-4 py-2 text-sm text-black placeholder:text-neutral-500 dark:border-neutral-800 dark:bg-transparent dark:text-white dark:placeholder:text-neutral-400 focus:ring-2 focus:ring-zinc-800"
                  />
                </div>
                <Button>Đăng nhập / Đăng ký</Button>
              </div>
            }
          />
        </div>
        <div className="flex justify-left items-center rounded-full gap-2 ring-1 ring-zinc-800">
          <Image
            src="/fridge.jpg"
            width={40}
            height={40}
            alt="logo"
            className="rounded-full"
          />
          <p className="pr-3 text-sm">renzynx</p>
        </div>
        <div className="relative min-w-[30%] lg:inline md:inline sm:hidden hidden">
          <span className="absolute inset-y-0 right-0 flex items-center pr-2">
            <IconSearch size={18} />
          </span>
          <input
            placeholder="Tìm kiếm..."
            className="w-full rounded-lg border bg-white px-4 py-2 text-sm text-black placeholder:text-neutral-500 dark:border-neutral-800 dark:bg-transparent dark:text-white dark:placeholder:text-neutral-400 focus:ring-2 focus:ring-zinc-800"
          />
        </div>
        <div>
          <ul className="flex justify-center items-center">
            <li className="lg:inline md:inline sm:hidden hidden">
              <Button className="mx-2">Đăng nhập / Đăng ký</Button>
            </li>

            <li className="mx-2 rounded-md flex justify-center items-center p-2 ring-1 ring-zinc-700 shadow-sm cursor-pointer hover:bg-zinc-900 ease-out">
              <Drawer
                dir="right"
                children={<>hello</>}
                controller={<IconShoppingCart />}
              />
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
